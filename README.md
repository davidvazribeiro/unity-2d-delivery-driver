# Unity 2D - Delivery Driver
## Description
Delivery Driver game from the "Complete C# Unity Game Developer 2D" course available at https://www.udemy.com/.


## Visuals
![Alt text](delivery_driver.png)

## Installation and Usage
Made with Unity version 2021.2.7f1. To access open the Delivery Driver in Unity.


## Contributing
No other contributors allowed, besides the author.


## Authors and acknowledgment
David A. V. Ribeiro


## License
MIT License


## Project status
Finished.
